package es.indra.plugin.rotateImage;

import java.io.File;
import java.io.FileOutputStream;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;

import org.json.JSONArray;
import org.json.JSONException;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import org.apache.cordova.camera.FileHelper;

@TargetApi(19)
public class RotateImage extends CordovaPlugin
{
    private static final String LOG_TAG = "RotateImage";
    private CallbackContext callbackContext;

    public RotateImage() {

    }

    @Override
    public boolean execute (String action, JSONArray args, CallbackContext callbackContext) throws JSONException
    {
        if( action.equals("rotate") )
        {
            final RotateImage self = this;
            final String imagePath = args.optString(0);
            final Integer rotation = args.optInt(1);
            this.callbackContext = callbackContext;

            final Context context = cordova.getActivity().getApplicationContext();

            cordova.getActivity().runOnUiThread( new Runnable() {
                public void run()
                {
                    try{
                        Bitmap currentImage = BitmapFactory.decodeStream(FileHelper.getInputStreamFromUriString(imagePath, cordova), null, null);

                        Matrix matrix = new Matrix();
                        matrix.postRotate(rotation);

                        Bitmap rotatedImage = Bitmap.createBitmap(currentImage, 0, 0, currentImage.getWidth(), currentImage.getHeight(),
                                matrix, true);

                        FileOutputStream fos = null;
                        fos = new FileOutputStream(FileHelper.getRealPath(imagePath, cordova));
                        rotatedImage.compress(Bitmap.CompressFormat.PNG, 100, fos);

                    } catch(Exception e){
                        Log.d(LOG_TAG, e.getMessage());
                    }

                    // send success result to cordova
                    PluginResult result = new PluginResult(PluginResult.Status.OK);
                    result.setKeepCallback(false);
                    self.callbackContext.sendPluginResult(result);
                }
            });

            // send "no-result" result to delay result handling
            PluginResult pluginResult = new  PluginResult(PluginResult.Status.NO_RESULT);
            pluginResult.setKeepCallback(true);
            callbackContext.sendPluginResult(pluginResult);

            return true;
        }
        return false;
    }

    @Override
    public void onDestroy()
    {

    }
}