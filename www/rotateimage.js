var exec = require('cordova/exec');

var RotateImage =
{
    rotate : function( imagePath, rotation, success, error )
    {
    	exec(function(uri) {
    		success(uri);
		}, function(e) {
			error(e);
  		}, "RotateImage", "rotate", [imagePath, rotation])
    }
}

module.exports = RotateImage;
